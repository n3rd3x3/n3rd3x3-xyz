module.exports = {
  // where it all starts -- the site's root Notion page (required)
  rootNotionPageId: '0140ddbaadb84d3b833e70830aa7ed04',

  // if you want to restrict pages to a single notion workspace (optional)
  // (this should be a Notion ID; see the docs for how to extract this)
  rootNotionSpaceId: null,

  // basic site info (required)
  name: 'n3rd3x3',
  domain: 'n3rd3x3.xyz',
  author: 'n3rd3x3',

  // open graph metadata (optional)
  description: 'a website',
  socialImageTitle: 'n3rd3x3',
  socialImageSubtitle: 'Hello 👋',

  // social usernames (optional)
  twitter: 'n3rd3x3',
  github: 'n3rd3x3',
  telegram: 'n3rd3x3_news',

  // default notion icon and cover images for site-wide consistency (optional)
  // page-specific values will override these site-wide defaults
  defaultPageIcon: null,
  defaultPageCover: null,
  defaultPageCoverPosition: 0.5,

  // image CDN host to proxy all image requests through (optional)
  // NOTE: this requires you to set up an external image proxy
  imageCDNHost: null,

  // Utteranc.es comments via GitHub issue comments (optional)
  utterancesGitHubRepo: 'n3rd3x3/n3rd3x3.xyz',

  // whether or not to enable support for LQIP preview images (optional)
  // NOTE: this requires you to set up Google Firebase and add the environment
  // variables specified in .env.example
  isPreviewImageSupportEnabled: false,

  // map of notion page IDs to URL paths (optional)
  // any pages defined here will override their default URL paths
  // example:
  //
  // pageUrlOverrides: {
  //   '/foo': '067dd719a912471ea9a3ac10710e7fdf',
  //   '/bar': '0be6efce9daf42688f65c76b89f8eb27'
  // }
}
